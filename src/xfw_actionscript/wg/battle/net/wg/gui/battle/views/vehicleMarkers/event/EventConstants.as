package net.wg.gui.battle.views.vehicleMarkers.event
{
    import flash.filters.DropShadowFilter;

    public class EventConstants extends Object
    {

        public static const REPLY_FILTER:DropShadowFilter = new DropShadowFilter(0,0,0,1,14,14,2,2);

        public static const MARKER_FILTER:DropShadowFilter = new DropShadowFilter(0,0,0,1,26,26,2,2);

        public function EventConstants()
        {
            super();
        }
    }
}
