package net.wg.gui.battle.views.destroyTimers.components.notifiers
{
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import net.wg.infrastructure.interfaces.entity.IDisplayable;

    public interface INotifier extends IDisposable, IDisplayable
    {
    }
}
