package net.wg.gui.battle.views.destroyTimers.utils.pairs
{
    public class NotifierPairState extends Object
    {

        public static const INACTIVE:int = 0;

        public static const HIDDEN:int = 1;

        public static const NORMAL:int = 2;

        public static const COLLAPSED:int = 3;

        public function NotifierPairState()
        {
            super();
        }
    }
}
