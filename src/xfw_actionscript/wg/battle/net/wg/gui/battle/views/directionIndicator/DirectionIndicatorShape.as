package net.wg.gui.battle.views.directionIndicator
{
    public class DirectionIndicatorShape extends Object
    {

        public static const SHAPE_GREEN:String = "green";

        public static const SHAPE_RED:String = "red";

        public static const SHAPE_PURPLE:String = "purple";

        public static const SHAPE_YELLOW:String = "yellow";

        public static const EVENT_SHAPE_FULL:String = "eventFull";

        public static const EVENT_SHAPE_NOT_FULL:String = "eventNotFull";

        public function DirectionIndicatorShape()
        {
            super();
        }
    }
}
