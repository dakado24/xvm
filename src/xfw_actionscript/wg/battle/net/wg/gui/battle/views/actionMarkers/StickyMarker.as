package net.wg.gui.battle.views.actionMarkers
{
    import flash.display.Sprite;
    import net.wg.infrastructure.interfaces.entity.IDisposable;
    import flash.text.TextField;
    import flash.display.MovieClip;
    import flash.geom.Point;

    public class StickyMarker extends Sprite implements IDisposable
    {

        public var arrow:StickyArrow = null;

        public var txtLabel:TextField = null;

        public var targetHighlight:MovieClip = null;

        public function StickyMarker()
        {
            super();
            this.arrow.visible = false;
            this.targetHighlight.visible = false;
            this.txtLabel.visible = false;
        }

        public final function dispose() : void
        {
            this.arrow = null;
            this.txtLabel = null;
            this.targetHighlight = null;
        }

        public function setArrowPosition(param1:Point) : void
        {
            this.arrow.x = param1.x;
            this.arrow.y = param1.y;
        }

        public function setArrowRadius(param1:int) : void
        {
            this.arrow.setRadius(param1);
        }

        public function setArrowVisible(param1:Boolean) : void
        {
            this.arrow.visible = param1;
        }

        public function setTextLabelEnabled(param1:Boolean) : void
        {
            this.txtLabel.alpha = param1?1:0.0;
        }

        public function setTextLabelPosition(param1:Point) : void
        {
            this.txtLabel.x = param1.x;
            this.txtLabel.y = param1.y;
        }

        public function setTextLabelVisible(param1:Boolean) : void
        {
            this.txtLabel.visible = param1;
        }
    }
}
