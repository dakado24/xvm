package net.wg.gui.components.tooltips.inblocks.data
{
    public class GlowTextBlockVO extends TextBlockVO
    {

        public var distance:int = -1;

        public var angle:int = -1;

        public var color:int = -1;

        public var alpha:int = -1;

        public var blurX:int = -1;

        public var blurY:int = -1;

        public var strength:int = -1;

        public var quality:int = -1;

        public function GlowTextBlockVO(param1:Object)
        {
            super(param1);
        }
    }
}
