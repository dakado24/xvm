package net.wg.gui.components.battleRoyale.data
{
    public interface IVehicleWeakZonesVO
    {

        function get engineLabel() : String;

        function get ammunitionLabel() : String;

        function get weakZonesSrc() : String;
    }
}
