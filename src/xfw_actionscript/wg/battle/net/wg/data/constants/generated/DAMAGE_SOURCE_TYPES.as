package net.wg.data.constants.generated
{
    public class DAMAGE_SOURCE_TYPES extends Object
    {

        public static const HEAVY_TANK:String = "heavyTank";

        public static const MEDIUM_TANK:String = "mediumTank";

        public static const LIGHT_TANK:String = "lightTank";

        public static const AT_SPG:String = "AT-SPG";

        public static const SPG:String = "SPG";

        public static const ARTILLERY:String = "artillery";

        public static const AIRSTRIKE:String = "airstrike";

        public static const BOSS:String = "eventBigBossBot";

        public static const BOMBER:String = "eventBomberBot";

        public static const HUNTER:String = "eventHunterBot";

        public static const RUNNER:String = "eventRunnerBot";

        public static const SENTRY:String = "eventSentryBot";

        public static const TURRET:String = "eventTurretBot";

        public static const RUNNER_SHOOTER:String = "eventRunnerShooterBot";

        public static const DAMAGE_SOURCES:Array = [HEAVY_TANK,MEDIUM_TANK,LIGHT_TANK,AT_SPG,SPG,ARTILLERY,AIRSTRIKE,BOSS,BOMBER,HUNTER,RUNNER,SENTRY,TURRET,RUNNER_SHOOTER];

        public function DAMAGE_SOURCE_TYPES()
        {
            super();
        }
    }
}
