package net.wg.data.constants.generated
{
    public class BATTLE_CONSUMABLES_PANEL_TAGS extends Object
    {

        public static const WITHOUT_TAG:String = "withoutTag";

        public static const MED_KIT:String = "medkit";

        public static const REPAIR_KIT:String = "repairkit";

        public static const TRIGGER:String = "trigger";

        public static const STIMULATOR:String = "stimulator";

        public static const EVENT_NITRO:String = "event_nitro";

        public static const EVENT_PASSIVE_ABILITY:String = "event_passive_ability";

        public static const SUPER_SHELL:String = "superShell";

        public static const EVENT_BUFF:String = "eventBuff";

        public static const REPAIR_AND_CREW_HEAL:String = "hpRepairAndCrewHeal";

        public static const EVENT_ITEM:String = "eventItem";

        public static const INSTANT_RELOAD:String = "instantReload";

        public static const RESURRECT:String = "resurrect";

        public function BATTLE_CONSUMABLES_PANEL_TAGS()
        {
            super();
        }
    }
}
