package net.wg.data.constants.generated
{
    public class BATTLE_NOTIFICATIONS_TIMER_LINKAGES extends Object
    {

        public static const DESTROY_TIMER_UI:String = "destroyTimerUI";

        public static const SECONDARY_TIMER_UI:String = "secondaryTimerUI";

        public static const BATTLE_ROYALE_DESTROY_TIMER_UI:String = "BattleRoyaleDestroyTimerUI";

        public static const BATTLE_ROYALE_TIMER_UI:String = "BattleRoyaleTimerUI";

        public static const FIRE_ICON:String = "destroyTimerFireUI";

        public static const BERSERKER_ICON:String = "destroyTimerBerserkerUI";

        public static const RECOVERY_ZONE_ICON:String = "secondaryRecoveryZoneIconUI";

        public static const OVERTURNED_ICON:String = "destroyTimerOverTurnedUI";

        public static const HALF_OVERTURNED_ICON:String = "HalfOverTurnedIconUI";

        public static const DEATHZONE_ICON:String = "destroyTimerDeathZoneUI";

        public static const DAMAGING_DEATHZONE_ICON:String = "DamageZoneIconContentUI";

        public static const AIRSTRIKE_ICON:String = "destroyTimerAirStrikeUI";

        public static const UNDER_FIRE_ICON:String = "destroyTimerDeathLandingUI";

        public static const RECOVERY_ICON:String = "destroyTimerRecoveryUI";

        public static const DROWN_ICON:String = "destroyTimerDrownUI";

        public static const GAS_DRAFT_ICON:String = "destroyTimerGazDraftUI";

        public static const STUN_ICON:String = "secondaryStunIconUI";

        public static const BLOCKED_ICON:String = "secondaryBlockedIconUI";

        public static const SMOKE_ICON:String = "secondarySmokeIconUI";

        public static const INSPIRE_ICON:String = "secondaryInspireIconUI";

        public static const HEAL_POINT_ICON:String = "secondaryHealPointIconUI";

        public static const RECOVERY_ICON_CONTENT:String = "RecoverIconContentUI";

        public static const ORANGE_ZONE_ICON_CONTENT:String = "OrangeZoneIconContentUI";

        public static const DAMAGING_SMOKE_ICON:String = "DamagingSmokeIconUI";

        public static const EVENTWARNING_ICON:String = "destroyTimerEventWarningUI";

        public function BATTLE_NOTIFICATIONS_TIMER_LINKAGES()
        {
            super();
        }
    }
}
