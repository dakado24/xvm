package net.wg.data.constants.generated
{
    public class BATTLE_EFFICIENCY_TYPES extends Object
    {

        public static const ARMOR:String = "armor";

        public static const DAMAGE:String = "damage";

        public static const RAM:String = "ram";

        public static const BURN:String = "burn";

        public static const DESTRUCTION:String = "kill";

        public static const TEAM_DESTRUCTION:String = "teamKill";

        public static const DETECTION:String = "spotted";

        public static const ASSIST_TRACK:String = "assistTrack";

        public static const ASSIST_SPOT:String = "assistSpot";

        public static const CRITS:String = "crits";

        public static const CAPTURE:String = "capture";

        public static const DEFENCE:String = "defence";

        public static const ASSIST:String = "assist";

        public static const WORLD_COLLISION:String = "worldCollision";

        public static const RECEIVED_CRITS:String = "receivedCrits";

        public static const RECEIVED_DAMAGE:String = "receivedDamage";

        public static const RECEIVED_BURN:String = "receivedBurn";

        public static const RECEIVED_RAM:String = "receivedRam";

        public static const RECEIVED_WORLD_COLLISION:String = "receivedWorldCollision";

        public static const ASSIST_STUN:String = "assistStun";

        public static const STUN:String = "stun";

        public static const VEHICLE_RECOVERY:String = "vehicleRecovery";

        public static const ENEMY_SECTOR_CAPTURED:String = "enemySectorCaptured";

        public static const DESTRUCTIBLE_DAMAGED:String = "destructibleDamaged";

        public static const DESTRUCTIBLE_DESTROYED:String = "destructibleDestroyed";

        public static const DESTRUCTIBLES_DEFENDED:String = "destructiblesDefended";

        public static const DEFENDER_BONUS:String = "defenderBonus";

        public static const BASE_CAPTURE_BLOCKED:String = "baseCaptureBlocked";

        public static const ASSIST_BY_ABILITY:String = "assistByAbility";

        public static const DEATH_ZONE:String = "deathZone";

        public static const BERSERKER:String = "berserker";

        public static const SPAWNED_BOT_DMG:String = "spawnedBotDmg";

        public static const RECEIVED_DMG_BY_SPAWNED_BOT:String = "receivedDmgBySpawnedBot";

        public static const RECEIVED_BY_MINEFIELD:String = "receivedByMinefield";

        public static const DAMAGE_BY_MINEFIELD:String = "damageByMinefield";

        public static const RECEIVED_BY_SMOKE:String = "receivedBySmoke";

        public static const EVENT_DEATH_ON_PHASE_CHANGE:String = "eventDeathOnPhaseChange";

        public static const BUFFS_RATION:String = "buffsRation";

        public static const BUFFS_FUEL:String = "buffsFuel";

        public static const BUFFS_RATE_FIRE:String = "buffsRateFire";

        public static const BUFFS_CONVERSION_SPEED:String = "buffsConversionSpeed";

        public static const BUFFS_INCREASED_MAXIMUM_DAMAGE:String = "buffsIncreasedMaximumDamage";

        public static const BUFFS_DOUBLE_DAMAGE:String = "buffsDoubleDamage";

        public static const BUFFS_INCENDIARY_SHOT:String = "buffsIncendiaryShot";

        public static const BUFFS_VAMPIRIC_SHOT:String = "buffsVampiricShot";

        public static const BUFFS_CONSTANT_HP_REGENERATION:String = "buffsConstantHpRegeneration";

        public static const BUFFS_ARMOR:String = "buffsArmor";

        public static const BUFFS_SET:Array = [BUFFS_RATION,BUFFS_FUEL,BUFFS_RATE_FIRE,BUFFS_CONVERSION_SPEED,BUFFS_INCREASED_MAXIMUM_DAMAGE,BUFFS_DOUBLE_DAMAGE,BUFFS_INCENDIARY_SHOT,BUFFS_VAMPIRIC_SHOT,BUFFS_CONSTANT_HP_REGENERATION,BUFFS_ARMOR];

        public function BATTLE_EFFICIENCY_TYPES()
        {
            super();
        }
    }
}
