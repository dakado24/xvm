package
{
    public class EVENT extends Object
    {

        public static const PUNISHMENTWINDOW_REASON_EVENT_DESERTER:String = "#event:punishmentWindow/reason/event_deserter";

        public static const PUNISHMENTWINDOW_REASON_EVENT_AFK:String = "#event:punishmentWindow/reason/event_afk";

        public static const NOTIFICATION_TESTMESSAGE:String = "#event:notification/testMessage";

        public static const NOTIFICATION_TESTMESSAGEDYNAMIC:String = "#event:notification/testMessageDynamic";

        public static const NOTIFICATION_DELIVERSOULS:String = "#event:notification/deliverSouls";

        public static const NOTIFICATION_COLLECTORENABLED:String = "#event:notification/collectorEnabled";

        public static const NOTIFICATION_BATTLEENDED:String = "#event:notification/battleEnded";

        public static const NOTIFICATION_FAILEDACTIVATECOLLECTOR:String = "#event:notification/failedActivateCollector";

        public static const NOTIFICATION_TIMEOUT:String = "#event:notification/timeOut";

        public static const NOTIFICATION_FAILEDRUNINTOCOLLECTOR:String = "#event:notification/failedRunIntoCollector";

        public static const NOTIFICATION_WORLD1:String = "#event:notification/world1";

        public static const NOTIFICATION_WORLD2:String = "#event:notification/world2";

        public static const NOTIFICATION_WORLD3:String = "#event:notification/world3";

        public static const NOTIFICATION_WORLD4:String = "#event:notification/world4";

        public static const NOTIFICATION_VICTORY:String = "#event:notification/victory";

        public static const NOTIFICATION_WORLD5:String = "#event:notification/world5";

        public static const RADIALMENU_CHATABOUT:String = "#event:radialMenu/chatAbout";

        public static const RADIALMENU_CHATWITH:String = "#event:radialMenu/chatWith";

        public static const RADIALMENU_ALLIES:String = "#event:radialMenu/allies";

        public static const RADIALMENU_NEXTPAGE:String = "#event:radialMenu/nextPage";

        public static const STYLESSHOP_DONE:String = "#event:stylesShop/done";

        public static const SHOP_MAINBANNER_TITLE:String = "#event:shop/mainBanner/title";

        public static const SHOP_MAINBANNER_DESCRIPTION:String = "#event:shop/mainBanner/description";

        public static const SHOP_MAINBANNER_DESCRIPTIONFULL:String = "#event:shop/mainBanner/descriptionFull";

        public static const SHOP_ITEMSBANNER_TITLE:String = "#event:shop/itemsBanner/title";

        public static const SHOP_ITEMSBANNER_DESCRIPTION:String = "#event:shop/itemsBanner/description";

        public static const SHOP_EVENTCOINSTITLE:String = "#event:shop/eventCoinsTitle";

        public static const TRADESTYLES_INFOTITLE:String = "#event:tradeStyles/infoTitle";

        public static const TRADESTYLES_INFODESCR:String = "#event:tradeStyles/infoDescr";

        public static const TRADESTYLES_INFOTITLENEWSTYLE:String = "#event:tradeStyles/infoTitleNewStyle";

        public static const TRADESTYLES_INFODESCRNEWSTYLE:String = "#event:tradeStyles/infoDescrNewStyle";

        public static const TRADESTYLES_SKINNAME:String = "#event:tradeStyles/skinName";

        public static const TRADESTYLES_CAROUSELSKINNAME:String = "#event:tradeStyles/carouselSkinName";

        public static const TRADESTYLES_ONLYFOR:String = "#event:tradeStyles/onlyFor";

        public static const TRADESTYLES_BUNDLE:String = "#event:tradeStyles/bundle";

        public static const TRADESTYLES_SKINDESCR:String = "#event:tradeStyles/skinDescr";

        public static const TRADESTYLES_SKINDONE:String = "#event:tradeStyles/skinDone";

        public static const TRADESTYLES_BUTTONCUSTOMIZATION:String = "#event:tradeStyles/buttonCustomization";

        public static const TRADESTYLES_BUTTONSTORAGE:String = "#event:tradeStyles/buttonStorage";

        public static const TRADESTYLES_CONFIRMATIONTITLE:String = "#event:tradeStyles/confirmationTitle";

        public static const TRADESTYLES_CONFIRMATIONMULTITITLE:String = "#event:tradeStyles/confirmationMultiTitle";

        public static const TRADESTYLES_WARNING:String = "#event:tradeStyles/warning";

        public static const TRADESTYLES_WARNINGMULTI:String = "#event:tradeStyles/warningMulti";

        public static const TRADESTYLES_WARNINGMULTIHASNOTALLTANKS:String = "#event:tradeStyles/warningMultiHasNotAllTanks";

        public static const TRADESTYLES_WARNINGMULTIHASTANK:String = "#event:tradeStyles/warningMultiHasTank";

        public static const TRADESTYLES_WARNINGHASTANK:String = "#event:tradeStyles/warningHasTank";

        public static const TRADESTYLES_CONFIRMATIONCOST:String = "#event:tradeStyles/confirmationCost";

        public static const TRADESTYLES_BUY:String = "#event:tradeStyles/buy";

        public static const TRADESTYLES_BUYSTYLE:String = "#event:tradeStyles/buyStyle";

        public static const TRADESTYLES_BUYSTYLES:String = "#event:tradeStyles/buyStyles";

        public static const TRADESTYLES_CANCEL:String = "#event:tradeStyles/cancel";

        public static const TRADESTYLES_BACK:String = "#event:tradeStyles/back";

        public static const TRADESTYLES_CLOSE:String = "#event:tradeStyles/close";

        public static const TRADESTYLES_INFOUNIQUE:String = "#event:tradeStyles/infoUnique";

        public static const TRADESTYLES_INFOMAP:String = "#event:tradeStyles/infoMap";

        public static const TRADESTYLES_INFOMAPANY:String = "#event:tradeStyles/infoMapAny";

        public static const TRADESTYLES_INFOVEHTYPE:String = "#event:tradeStyles/infoVehType";

        public static const TRADESTYLES_INFOVEHTYPESTR:String = "#event:tradeStyles/infoVehTypeStr";

        public static const TRADESTYLES_INFOMODE:String = "#event:tradeStyles/infoMode";

        public static const TRADESTYLES_INFOMODEANY:String = "#event:tradeStyles/infoModeAny";

        public static const TRADESTYLES_INFOBONUS:String = "#event:tradeStyles/infoBonus";

        public static const TRADESTYLES_NOTENOUGH:String = "#event:tradeStyles/notEnough";

        public static const TRADESTYLES_NOTENOUGHTOKENSHEADER:String = "#event:tradeStyles/notEnoughTokensHeader";

        public static const TRADESTYLES_NOTENOUGHTOKENSBODY:String = "#event:tradeStyles/notEnoughTokensBody";

        public static const TRADESTYLES_NOTENOUGHGOLDHEADER:String = "#event:tradeStyles/notEnoughGoldHeader";

        public static const TRADESTYLES_NOTENOUGHGOLDBODY:String = "#event:tradeStyles/notEnoughGoldBody";

        public static const TRADESTYLES_REWARD:String = "#event:tradeStyles/reward";

        public static const RESULTSCREEN_TAB_0:String = "#event:resultScreen/tab/0";

        public static const RESULTSCREEN_TAB_1:String = "#event:resultScreen/tab/1";

        public static const RESULTSCREEN_STATS_MATTERONTANK:String = "#event:resultScreen/stats/matterOnTank";

        public static const RESULTSCREEN_STATS_MATTER:String = "#event:resultScreen/stats/matter";

        public static const RESULTSCREEN_STATS_DAMAGE:String = "#event:resultScreen/stats/damage";

        public static const RESULTSCREEN_STATS_KILL:String = "#event:resultScreen/stats/kill";

        public static const RESULTSCREEN_STATUS:String = "#event:resultScreen/status";

        public static const RESULTSCREEN_CLOSE:String = "#event:resultScreen/close";

        public static const RESULTSCREEN_ALIVE:String = "#event:resultScreen/alive";

        public static const RESULTSCREEN_DEAD:String = "#event:resultScreen/dead";

        public static const RESULTSCREEN_TIME:String = "#event:resultScreen/time";

        public static const RESULTSCREEN_TOOLTIPS_EXPHEADER:String = "#event:resultScreen/tooltips/expHeader";

        public static const RESULTSCREEN_TOOLTIPS_EXP:String = "#event:resultScreen/tooltips/exp";

        public static const RESULTSCREEN_TOOLTIPS_MATTERONTANKHEADER:String = "#event:resultScreen/tooltips/matterOnTankHeader";

        public static const RESULTSCREEN_TOOLTIPS_MATTERONTANK:String = "#event:resultScreen/tooltips/matterOnTank";

        public static const RESULTSCREEN_TOOLTIPS_MATTERHEADER:String = "#event:resultScreen/tooltips/matterHeader";

        public static const RESULTSCREEN_TOOLTIPS_MATTER:String = "#event:resultScreen/tooltips/matter";

        public static const RESULTSCREEN_COMMANDERDESCR:String = "#event:resultScreen/commanderDescr";

        public static const RESULTSCREEN_COLLECTMOREMATTER:String = "#event:resultScreen/collectMoreMatter";

        public static const RESULTSCREEN_QUESTCOMPLETED:String = "#event:resultScreen/questCompleted";

        public static const RESULTSCREEN_QUESTNOTCOMPLETED:String = "#event:resultScreen/questNotCompleted";

        public static const BATTLERESULTS_CAPTURESTATUS_0:String = "#event:battleResults/captureStatus/0";

        public static const BATTLERESULTS_CAPTURESTATUS_1:String = "#event:battleResults/captureStatus/1";

        public static const BATTLERESULTS_CAPTURESTATUS_2:String = "#event:battleResults/captureStatus/2";

        public static const BATTLERESULTS_CAPTURESTATUS_3:String = "#event:battleResults/captureStatus/3";

        public static const BATTLERESULTS_CAPTURESTATUS_4:String = "#event:battleResults/captureStatus/4";

        public static const COMMANDER_LABEL_1:String = "#event:commander_label_1";

        public static const COMMANDER_LABEL_2:String = "#event:commander_label_2";

        public static const COMMANDER_LABEL_3:String = "#event:commander_label_3";

        public static const COMMANDER_LABEL_4:String = "#event:commander_label_4";

        public static const COMMANDER_LABEL_5:String = "#event:commander_label_5";

        public static const BOSSAURA_LEAVEDAMAGEAREAWARNINGSOULS:String = "#event:bossAura/leaveDamageAreaWarningSouls";

        public static const BOSSAURA_LEAVEDAMAGEAREAWARNINGHEALTH:String = "#event:bossAura/leaveDamageAreaWarningHealth";

        public static const DESTROYTIMER_HALFOVERTURNED:String = "#event:destroyTimer/halfOverturned";

        public static const HANGAR_EVENTTANKRENT_DESCRIPTION:String = "#event:hangar/eventTankRent/description";

        public static const HANGAR_EVENTTANKRENT_BONUSES_LABEL:String = "#event:hangar/eventTankRent/bonuses/label";

        public static const HANGAR_EVENTTANKRENT_TOPACK:String = "#event:hangar/eventTankRent/toPack";

        public static const TANKPANEL_OR:String = "#event:tankPanel/or";

        public static const TANKPANEL_TOQUESTS:String = "#event:tankPanel/toQuests";

        public static const HANGAR_QUESTS_HEADER:String = "#event:hangar/quests/header";

        public static const HANGAR_QUESTS_CALENDAR:String = "#event:hangar/quests/calendar";

        public static const HANGAR_HEADER_LABEL:String = "#event:hangar/header/label";

        public static const EVENT_CLOSEBTN:String = "#event:event/closeBtn";

        public static const EVENT_SELECT_DIFFICULTY:String = "#event:event/select_difficulty";

        public static const EVENT_SQUAD_DIFFICULTY:String = "#event:event/squad_difficulty";

        public static const EVENT_DIFFICULTY_SELECTED:String = "#event:event/difficulty_selected";

        public static const EVENT_DIFFICULTY_ENABLED:String = "#event:event/difficulty_enabled";

        public static const EVENT_DIFFICULTY_LEVEL_1_REWARD_DESC:String = "#event:event/difficulty/level/1/reward_desc";

        public static const EVENT_DIFFICULTY_LEVEL_2_REWARD_DESC:String = "#event:event/difficulty/level/2/reward_desc";

        public static const EVENT_DIFFICULTY_LEVEL_3_REWARD_DESC:String = "#event:event/difficulty/level/3/reward_desc";

        public static const EVENT_DIFFICULTY_UNLOCK_CONDITION:String = "#event:event/difficulty/unlock_condition";

        public static const EVENT_DIFFICULTY_MAX_DIFFICULTY_LEVEL:String = "#event:event/difficulty/max_difficulty_level";

        public static const EVENT_DIFFICULTY_SQUAD_PLAYER_MAX_DIFFICULTY_LEVEL:String = "#event:event/difficulty/squad_player_max_difficulty_level";

        public static const EVENT_DIFFICULTY_DISABLED_DIFFICULTY_LEVEL:String = "#event:event/difficulty/disabled_difficulty_level";

        public static const EVENT_DIFFICULTY_DISABLED:String = "#event:event/difficulty_disabled";

        public static const EVENT_DIFFICULTY_LEVEL_1_TITLE:String = "#event:event/difficulty/level/1/title";

        public static const EVENT_DIFFICULTY_LEVEL_2_TITLE:String = "#event:event/difficulty/level/2/title";

        public static const EVENT_DIFFICULTY_LEVEL_3_TITLE:String = "#event:event/difficulty/level/3/title";

        public static const EVENT_DIFFICULTY_LEVEL_1_NUM_PHASES:String = "#event:event/difficulty/level/1/num_phases";

        public static const EVENT_DIFFICULTY_LEVEL_2_NUM_PHASES:String = "#event:event/difficulty/level/2/num_phases";

        public static const EVENT_DIFFICULTY_LEVEL_3_NUM_PHASES:String = "#event:event/difficulty/level/3/num_phases";

        public static const EVENT_DIFFICULTY_UNLOCK_TITLE:String = "#event:event/difficulty/unlock_title";

        public static const EVENT_DIFFICULTY_UNLOCK_DESCRIPTION1:String = "#event:event/difficulty/unlock_description1";

        public static const EVENT_DIFFICULTY_UNLOCK_DESCRIPTION2:String = "#event:event/difficulty/unlock_description2";

        public static const EVENT_DIFFICULTY_CHANGE:String = "#event:event/difficulty/change";

        public static const EVENT_DIFFICULTY_CONFIRM:String = "#event:event/difficulty/confirm";

        public static const HANGAR_COINS:String = "#event:hangar/coins";

        public static const EVENT_BONUSES_DESCRIPTION:String = "#event:event/bonuses/description";

        public static const EVENT_BONUSES_TOLEVEL:String = "#event:event/bonuses/toLevel";

        public static const EVENT_BONUSES_MAXLEVEL:String = "#event:event/bonuses/maxLevel";

        public static const EVENT_BONUSES_FORLEVEL:String = "#event:event/bonuses/forLevel";

        public static const EVENT_BONUSES_TOMODE:String = "#event:event/bonuses/toMode";

        public static const EVENT_BONUSES_MODES:String = "#event:event/bonuses/Modes";

        public static const EVENT_BONUSES_LEARNEDSKILLS:String = "#event:event/bonuses/LearnedSkills";

        public static const TOOLTIP_VEHICLE_PREMIUM_RENT_DESCRIPTION1:String = "#event:tooltip/vehicle/premium_rent/description1";

        public static const TOOLTIP_VEHICLE_PREMIUM_RENT_DESCRIPTION2:String = "#event:tooltip/vehicle/premium_rent/description2";

        public static const TOOLTIP_VEHICLE_PREMIUM_RENT_HOWTOGET:String = "#event:tooltip/vehicle/premium_rent/howToGet";

        public static const TOOLTIP_VEHICLE_PREMIUM_RENT_HOWTOGETDESC:String = "#event:tooltip/vehicle/premium_rent/howToGetDesc";

        public static const TOOLTIP_VEHICLE_PREMIUM_RENT_DESCRIPTION3:String = "#event:tooltip/vehicle/premium_rent/description3";

        public static const TOOLTIP_VEHICLE_REPAIR_HEADER:String = "#event:tooltip/vehicle/repair/header";

        public static const TOOLTIP_VEHICLE_REPAIR_DESCRIPTION1:String = "#event:tooltip/vehicle/repair/description1";

        public static const TOOLTIP_VEHICLE_BOOSTER_HEADER:String = "#event:tooltip/vehicle/booster/header";

        public static const TOOLTIP_VEHICLE_BOOSTER_DESCRIPTION1:String = "#event:tooltip/vehicle/booster/description1";

        public static const TOOLTIP_VEHICLE_BOOSTER_DESCRIPTION2:String = "#event:tooltip/vehicle/booster/description2";

        public static const TOOLTIP_VEHICLE_BOOSTER_DESCRIPTION3:String = "#event:tooltip/vehicle/booster/description3";

        public static const TOOLTIP_BUFFS_MULTIPLYSHOTDISPERSION_TITLE:String = "#event:tooltip/buffs/multiplyShotDispersion/title";

        public static const TOOLTIP_BUFFS_MULTIPLYSHOTDISPERSION_DESCRIPTION:String = "#event:tooltip/buffs/multiplyShotDispersion/description";

        public static const TOOLTIP_BUFFS_HEALONCEONSHOT_TITLE:String = "#event:tooltip/buffs/healOnceOnShot/title";

        public static const TOOLTIP_BUFFS_HEALONCEONSHOT_DESCRIPTION:String = "#event:tooltip/buffs/healOnceOnShot/description";

        public static const TOOLTIP_BUFFS_MULTIPLYDAMAGEBY10_TITLE:String = "#event:tooltip/buffs/multiplyDamageBy10/title";

        public static const TOOLTIP_BUFFS_MULTIPLYDAMAGEBY10_DESCRIPTION:String = "#event:tooltip/buffs/multiplyDamageBy10/description";

        public static const TOOLTIP_BUFFS_MULTIPLYGUNRELOADTIME_TITLE:String = "#event:tooltip/buffs/multiplyGunReloadTime/title";

        public static const TOOLTIP_BUFFS_MULTIPLYGUNRELOADTIME_DESCRIPTION:String = "#event:tooltip/buffs/multiplyGunReloadTime/description";

        public static const TOOLTIP_BUFFS_ARMOR_TITLE:String = "#event:tooltip/buffs/armor/title";

        public static const TOOLTIP_BUFFS_ARMOR_DESCRIPTION:String = "#event:tooltip/buffs/armor/description";

        public static const TOOLTIP_BUFFS_RATION_TITLE:String = "#event:tooltip/buffs/ration/title";

        public static const TOOLTIP_BUFFS_RATION_DESCRIPTION:String = "#event:tooltip/buffs/ration/description";

        public static const TOOLTIP_BUFFS_FUEL_TITLE:String = "#event:tooltip/buffs/fuel/title";

        public static const TOOLTIP_BUFFS_FUEL_DESCRIPTION:String = "#event:tooltip/buffs/fuel/description";

        public static const TOOLTIP_BUFFS_DAMAGEONCEONSHOT_TITLE:String = "#event:tooltip/buffs/damageOnceOnShot/title";

        public static const TOOLTIP_BUFFS_DAMAGEONCEONSHOT_DESCRIPTION:String = "#event:tooltip/buffs/damageOnceOnShot/description";

        public static const TOOLTIP_BUFFS_REGENERATIONHP_TITLE:String = "#event:tooltip/buffs/regenerationHP/title";

        public static const TOOLTIP_BUFFS_REGENERATIONHP_DESCRIPTION:String = "#event:tooltip/buffs/regenerationHP/description";

        public static const TOOLTIP_BUFFS_IGNITEONSHOT_TITLE:String = "#event:tooltip/buffs/igniteOnShot/title";

        public static const TOOLTIP_BUFFS_IGNITEONSHOT_DESCRIPTION:String = "#event:tooltip/buffs/igniteOnShot/description";

        public static const TOOLTIP_MENU_DIFFICULTY_HEADER:String = "#event:tooltip/menu/difficulty/header";

        public static const TOOLTIP_MENU_DIFFICULTY_DESCRIPTION:String = "#event:tooltip/menu/difficulty/description";

        public static const TOOLTIP_MENU_DIFFICULTY_HEADER_LOCKED:String = "#event:tooltip/menu/difficulty/header_locked";

        public static const TOOLTIP_MENU_DIFFICULTY_DESCRIPTION_LOCKED:String = "#event:tooltip/menu/difficulty/description_locked";

        public static const TOOLTIP_BUFFS_ANOMALY:String = "#event:tooltip/buffs/anomaly";

        public static const HANGAR_HEADER_BASE:String = "#event:hangar/header/base";

        public static const HANGAR_HEADER_MISSIONS:String = "#event:hangar/header/missions";

        public static const HANGAR_HEADER_SHOP:String = "#event:hangar/header/shop";

        public static const HANGAR_HEADER_ABOUT:String = "#event:hangar/header/about";

        public static const HANGAR_HEADER_NOTES:String = "#event:hangar/header/notes";

        public static const HANGAR_HEADER_DIFFICULTY:String = "#event:hangar/header/difficulty";

        public static const HANGAR_REPAIR_VEHICLE:String = "#event:hangar/repair_vehicle";

        public static const HANGAR_CREW_HEALING:String = "#event:hangar/crew_healing";

        public static const HANGAR_CREW_BOOSTER_CONFIRM_BUY_LABEL:String = "#event:hangar/crew_booster/confirm_buy/label";

        public static const HANGAR_CREW_BOOSTER_CONFIRM_BUY_MESSAGE:String = "#event:hangar/crew_booster/confirm_buy/message";

        public static const HANGAR_CREW_BOOSTER_CONFIRM_BUY_LABELEXECUTE:String = "#event:hangar/crew_booster/confirm_buy/labelExecute";

        public static const HANGAR_CREW_BOOSTER_CONFIRM_BUY_LABELBUY:String = "#event:hangar/crew_booster/confirm_buy/labelBuy";

        public static const HANGAR_CREW_BOOSTER_CONFIRM_BUY_INSTORAGE:String = "#event:hangar/crew_booster/confirm_buy/inStorage";

        public static const HANGAR_CREW_HEALING_CONFIRM_BUY_LABEL:String = "#event:hangar/crew_healing/confirm_buy/label";

        public static const HANGAR_CREW_HEALING_CONFIRM_BUY_MESSAGE:String = "#event:hangar/crew_healing/confirm_buy/message";

        public static const HANGAR_CREW_HEALING_CONFIRM_BUY_LABELEXECUTE:String = "#event:hangar/crew_healing/confirm_buy/labelExecute";

        public static const HANGAR_CREW_HEALING_CONFIRM_BUY_LABELBUY:String = "#event:hangar/crew_healing/confirm_buy/labelBuy";

        public static const HANGAR_CREW_HEALING_CONFIRM_TRAIN_LABEL:String = "#event:hangar/crew_healing/confirm_train/label";

        public static const HANGAR_CREW_HEALING_CONFIRM_TRAIN_LABELEXECUTE:String = "#event:hangar/crew_healing/confirm_train/labelExecute";

        public static const HANGAR_CREW_HEALING_CONFIRM_TRAIN_MESSAGE:String = "#event:hangar/crew_healing/confirm_train/message";

        public static const HANGAR_CREW_HEALING_PANEL_HEADER:String = "#event:hangar/crew_healing/panel/header";

        public static const HANGAR_CREW_HEALING_PANEL_BUTTONLABEL:String = "#event:hangar/crew_healing/panel/buttonLabel";

        public static const HANGAR_CREW_HEALING_PANEL_TIMELEFT:String = "#event:hangar/crew_healing/panel/timeLeft";

        public static const HANGAR_CREW_BOOSTER_PANEL_HEADER:String = "#event:hangar/crew_booster/panel/header";

        public static const HANGAR_CREW_BOOSTER_PANEL_BUTTONLABEL:String = "#event:hangar/crew_booster/panel/buttonLabel";

        public static const HANGAR_CREW_BOOSTER_PANEL_ACTIVATED:String = "#event:hangar/crew_booster/panel/activated";

        public static const HANGAR_CREW_BOOSTER_PANEL_INSTORAGE:String = "#event:hangar/crew_booster/panel/inStorage";

        public static const HANGAR_CREW_PREMIUM_LABEL:String = "#event:hangar/crew_premium/label";

        public static const HANGAR_BANNER_DATE_INFO:String = "#event:hangar/banner/date_info";

        public static const HANGAR_BANNER_BAN_INFO:String = "#event:hangar/banner/ban_info";

        public static const HANGAR_BAN_DESCRIPTION:String = "#event:hangar/ban/description";

        public static const HANGAR_BANNER_HEADER_LABEL:String = "#event:hangar/banner/header/label";

        public static const NOTIFICATION_HUNTER:String = "#event:notification/hunter";

        public static const NOTIFICATION_BOMBER:String = "#event:notification/bomber";

        public static const NOTIFICATION_TURRET:String = "#event:notification/turret";

        public static const NOTIFICATION_FINAL_HINT:String = "#event:notification/final_hint";

        public static const NOTIFICATION_ANOMALY_POWER:String = "#event:notification/anomaly_power";

        public static const NOTIFICATION_ANOMALY_RAGE:String = "#event:notification/anomaly_rage";

        public static const NOTIFICATION_ANOMALY_STRENGTH:String = "#event:notification/anomaly_strength";

        public static const NOTIFICATION_ANOMALY_ACCURACY:String = "#event:notification/anomaly_accuracy";

        public static const MISSIONS_STATUS_COMPLETED:String = "#event:missions/status/completed";

        public static const MISSIONS_STATUS_LOCKED:String = "#event:missions/status/locked";

        public static const MISSIONS_STATUS_AVAILABLE:String = "#event:missions/status/available";

        public static const MISSIONS_STATUS_DAILYAVAILABLE:String = "#event:missions/status/dailyAvailable";

        public static const MISSIONS_STATUS_DAILYCOMPLETED:String = "#event:missions/status/dailyCompleted";

        public static const MISSIONS_STATUS_WILLBEAVAILABLEAT:String = "#event:missions/status/willBeAvailableAt";

        public static const MISSIONS_PANEL_DAILYCOMPLETED:String = "#event:missions/panel/dailyCompleted";

        public static const MISSIONS_PANEL_ALLCOMPLETED:String = "#event:missions/panel/allCompleted";

        public static const MISSIONS_MISSION_1_HEADER:String = "#event:missions/mission/1/header";

        public static const MISSIONS_MISSION_1_ITEM_1_DESC:String = "#event:missions/mission/1/item/1/desc";

        public static const MISSIONS_MISSION_2_HEADER:String = "#event:missions/mission/2/header";

        public static const MISSIONS_MISSION_2_ITEM_1_DESC:String = "#event:missions/mission/2/item/1/desc";

        public static const MISSIONS_MISSION_2_ITEM_2_DESC:String = "#event:missions/mission/2/item/2/desc";

        public static const MISSIONS_MISSION_2_ITEM_3_DESC:String = "#event:missions/mission/2/item/3/desc";

        public static const MISSIONS_MISSION_2_ITEM_4_DESC:String = "#event:missions/mission/2/item/4/desc";

        public static const MISSIONS_MISSION_2_ITEM_5_DESC:String = "#event:missions/mission/2/item/5/desc";

        public static const MISSIONS_MISSION_3_HEADER:String = "#event:missions/mission/3/header";

        public static const MISSIONS_MISSION_3_ITEM_1_DESC:String = "#event:missions/mission/3/item/1/desc";

        public static const MISSIONS_MISSION_3_ITEM_2_DESC:String = "#event:missions/mission/3/item/2/desc";

        public static const MISSIONS_MISSION_3_ITEM_3_DESC:String = "#event:missions/mission/3/item/3/desc";

        public static const MISSIONS_MISSION_3_ITEM_4_DESC:String = "#event:missions/mission/3/item/4/desc";

        public static const MISSIONS_MISSION_3_ITEM_5_DESC:String = "#event:missions/mission/3/item/5/desc";

        public static const MISSIONS_TOOLTIPHEADER:String = "#event:missions/tooltipHeader";

        public static const MISSIONS_TOOLTIPBODY:String = "#event:missions/tooltipBody";

        public static const STATS_WORLD_0_TITLE:String = "#event:stats/world/0/title";

        public static const STATS_WORLD_0_DESC:String = "#event:stats/world/0/desc";

        public static const STATS_WORLD_1_TITLE:String = "#event:stats/world/1/title";

        public static const STATS_WORLD_1_DESC:String = "#event:stats/world/1/desc";

        public static const STATS_WORLD_2_TITLE:String = "#event:stats/world/2/title";

        public static const STATS_WORLD_2_DESC:String = "#event:stats/world/2/desc";

        public static const STATS_WORLD_3_TITLE:String = "#event:stats/world/3/title";

        public static const STATS_WORLD_3_DESC:String = "#event:stats/world/3/desc";

        public static const STATS_DIFFICULTY:String = "#event:stats/difficulty";

        public static const VEHICLETOOLTIP_HEADER_VEHICLETYPE_EVENT_LIGHTTANK:String = "#event:vehicleTooltip/header/vehicleType/event/lightTank";

        public static const VEHICLETOOLTIP_HEADER_VEHICLETYPE_EVENT_MEDIUMTANK:String = "#event:vehicleTooltip/header/vehicleType/event/mediumTank";

        public static const VEHICLETOOLTIP_HEADER_VEHICLETYPE_EVENT_HEAVYTANK:String = "#event:vehicleTooltip/header/vehicleType/event/heavyTank";

        public static const VEHICLETOOLTIP_HEADER_VEHICLETYPE_EVENT_AT_SPG:String = "#event:vehicleTooltip/header/vehicleType/event/AT-SPG";

        public static const VEHICLETOOLTIP_HEADER_VEHICLETYPE_EVENT_SPG:String = "#event:vehicleTooltip/header/vehicleType/event/SPG";

        public static const VEHICLETOOLTIP_HEADER_DOUBLEXP:String = "#event:vehicleTooltip/header/doubleXP";

        public static const SHOP_SIMPLEITEM_TITLE:String = "#event:shop/simpleItem/title";

        public static const SHOP_SIMPLEITEM_INSTOCK:String = "#event:shop/simpleItem/inStock";

        public static const SHOP_SIMPLEITEM_AVAILABLEFORPURCHASE:String = "#event:shop/simpleItem/availableForPurchase";

        public static const SHOP_SIMPLEITEM_BACKDESCRIPTION:String = "#event:shop/simpleItem/backDescription";

        public static const SHOP_SIMPLEITEM_BTNLABEL:String = "#event:shop/simpleItem/btnLabel";

        public static const SHOP_SIMPLEITEM_OUTOFSTOCK:String = "#event:shop/simpleItem/outOfStock";

        public static const SHOP_SIMPLEITEM_OUTOFSTOCKMSG:String = "#event:shop/simpleItem/outOfStockMsg";

        public static const SHOP_SIMPLEITEMCONGRATULATION_HEADER:String = "#event:shop/simpleItemCongratulation/header";

        public static const SHOP_SIMPLEITEMCONGRATULATION_DESCRIPTION:String = "#event:shop/simpleItemCongratulation/description";

        public static const SHOP_SIMPLEITEMCONGRATULATION_BTNLABEL:String = "#event:shop/simpleItemCongratulation/btnLabel";

        public static const SHOP_SIMPLEITEMFORTOKENS_BOOSTER_XP_100:String = "#event:shop/simpleItemForTokens/booster_xp_100";

        public static const SHOP_SIMPLEITEMFORTOKENS_BOOSTER_XP_50:String = "#event:shop/simpleItemForTokens/booster_xp_50";

        public static const SHOP_SIMPLEITEMFORTOKENS_BOOSTER_CREDITS_50:String = "#event:shop/simpleItemForTokens/booster_credits_50";

        public static const SHOP_SIMPLEITEMFORTOKENS_BOOSTER_CREDITS_25:String = "#event:shop/simpleItemForTokens/booster_credits_25";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BOOSTER_CREDITS_50:String = "#event:shop/simpleItem/description/booster_credits_50";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BOOSTER_CREDITS_25:String = "#event:shop/simpleItem/description/booster_credits_25";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BOOSTER_XP_100:String = "#event:shop/simpleItem/description/booster_xp_100";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BOOSTER_XP_50:String = "#event:shop/simpleItem/description/booster_xp_50";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BROCHURE_FRANCE:String = "#event:shop/simpleItem/description/brochure_france";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BROCHURE_GERMANY:String = "#event:shop/simpleItem/description/brochure_germany";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BROCHURE_UK:String = "#event:shop/simpleItem/description/brochure_uk";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BROCHURE_USA:String = "#event:shop/simpleItem/description/brochure_usa";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_BROCHURE_USSR:String = "#event:shop/simpleItem/description/brochure_ussr";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_COATEDOPTICS:String = "#event:shop/simpleItem/description/coatedOptics";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_COMMANDER_SIXTHSENSE:String = "#event:shop/simpleItem/description/commander_sixthSense";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_ENHANCEDAIMDRIVES:String = "#event:shop/simpleItem/description/enhancedAimDrives";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_FIREFIGHTING:String = "#event:shop/simpleItem/description/fireFighting";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_GUIDE_FRANCE:String = "#event:shop/simpleItem/description/guide_france";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_GUIDE_GERMANY:String = "#event:shop/simpleItem/description/guide_germany";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_GUIDE_UK:String = "#event:shop/simpleItem/description/guide_uk";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_GUIDE_USA:String = "#event:shop/simpleItem/description/guide_usa";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_GUIDE_USSR:String = "#event:shop/simpleItem/description/guide_ussr";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_IMPROVEDVENTILATION:String = "#event:shop/simpleItem/description/improvedVentilation";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_LOADER_PEDANT:String = "#event:shop/simpleItem/description/loader_pedant";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_NATURALCOVER:String = "#event:shop/simpleItem/description/naturalCover";

        public static const SHOP_SIMPLEITEM_DESCRIPTION_UNIVERSALBOOK:String = "#event:shop/simpleItem/description/universalBook";

        public static const SHOP_DISCOUNT:String = "#event:shop/discount";

        public static const SHOP_BESTDEAL_0:String = "#event:shop/bestDeal/0";

        public static const SHOP_BESTDEAL_1:String = "#event:shop/bestDeal/1";

        public static const SHOP_BESTDEALCONFIRMATION_0:String = "#event:shop/bestDealConfirmation/0";

        public static const SHOP_BESTDEALCONFIRMATION_1:String = "#event:shop/bestDealConfirmation/1";

        public static const SHOP_BESTDEAL_0_DESCRIPTION:String = "#event:shop/bestDeal/0/description";

        public static const SHOP_BESTDEAL_1_DESCRIPTION:String = "#event:shop/bestDeal/1/description";

        public static const SHOP_BESTDEAL_0_GIFTDESCR:String = "#event:shop/bestDeal/0/giftDescr";

        public static const SHOP_BESTDEAL_1_GIFTDESCR:String = "#event:shop/bestDeal/1/giftDescr";

        public static const SHOP_BESTDEAL_0_EXTRABONUSDESCR:String = "#event:shop/bestDeal/0/extraBonusDescr";

        public static const SHOP_BESTDEAL_1_EXTRABONUSDESCR:String = "#event:shop/bestDeal/1/extraBonusDescr";

        public static const SHOP_BESTDEAL_CONGRATS_HE19_R40_T_54_HALLOWEEN:String = "#event:shop/bestDeal/congrats/he19_R40_T_54_Halloween";

        public static const SHOP_BESTDEAL_CONGRATS_HE19_A100_T49_HALLOWEEN:String = "#event:shop/bestDeal/congrats/he19_A100_T49_Halloween";

        public static const SHOP_BESTDEAL_HE19_R40_T_54_HALLOWEEN:String = "#event:shop/bestDeal/he19_R40_T_54_Halloween";

        public static const SHOP_BESTDEAL_HE19_A100_T49_HALLOWEEN:String = "#event:shop/bestDeal/he19_A100_T49_Halloween";

        public static const SHOP_AVAILABLE:String = "#event:shop/available";

        public static const SHOP_BOUGHT:String = "#event:shop/bought";

        public static const SHOP_BESTDEAL_BOUGHT:String = "#event:shop/bestDeal/bought";

        public static const SHOP_BESTDEAL_BATTLEBOOSTER:String = "#event:shop/bestDeal/battleBooster";

        public static const SHOP_BESTDEAL_CREWBOOKS:String = "#event:shop/bestDeal/crewBooks";

        public static const SHOP_BESTDEAL_GOODIES:String = "#event:shop/bestDeal/goodies";

        public static const SHOP_BESTDEAL_HE20_SHOP_ITEMS_STYLE:String = "#event:shop/bestDeal/he20_shop_items_style";

        public static const SHOP_BESTDEAL_TOKEN_HE19_ENERGY_FOR_USE_HEALING:String = "#event:shop/bestDeal/token/he19_energy_for_use_healing";

        public static const SHOP_BESTDEAL_TOKEN_HE19_ENERGY_FOR_USE_BOOSTER:String = "#event:shop/bestDeal/token/he19_energy_for_use_booster";

        public static const SHOP_BESTDEAL_PREMIUM_PLUS:String = "#event:shop/bestDeal/premium_plus";

        public static const SHOP_BESTDEAL_BACKDESCRIPTION:String = "#event:shop/bestDeal/backDescription";

        public static const SHOP_BESTDEAL_BUYBUTTONLABEL:String = "#event:shop/bestDeal/buyButtonLabel";

        public static const SHOP_BESTDEAL_FREE:String = "#event:shop/bestDeal/free";

        public static const SHOP_BESTDEAL_0_FREEDESCRIPTION:String = "#event:shop/bestDeal/0/freeDescription";

        public static const SHOP_BESTDEAL_1_FREEDESCRIPTION:String = "#event:shop/bestDeal/1/freeDescription";

        public static const SHOP_BESTDEAL_TOOLTIP_INFO_0:String = "#event:shop/bestDeal/tooltip/info/0";

        public static const SHOP_BESTDEAL_TOOLTIP_INFO_1:String = "#event:shop/bestDeal/tooltip/info/1";

        public static const SHOP_BESTDEAL_PREMIUMVEHICLESDESCRIPTION:String = "#event:shop/bestDeal/premiumVehiclesDescription";

        public static const SHOP_BESTDEAL_ITEMCOUNT:String = "#event:shop/bestDeal/itemCount";

        public static const SHOP_STYLEBUY_TITLE:String = "#event:shop/styleBuy/title";

        public static const SHOP_STYLEBUY_GIFTTITLE:String = "#event:shop/styleBuy/giftTitle";

        public static const SHOP_STYLEBUY_GIFTDESCR:String = "#event:shop/styleBuy/giftDescr";

        public static const SHOP_RENTTANK_TITLE:String = "#event:shop/rentTank/title";

        public static const SHOP_RENTTANK_DESCR:String = "#event:shop/rentTank/descr";

        public static const SHOP_RENTTANK_GIFTTITLE:String = "#event:shop/rentTank/giftTitle";

        public static const SHOP_RENTTANK_GIFTDESCR:String = "#event:shop/rentTank/giftDescr";

        public static const SHOP_ENDDATE:String = "#event:shop/endDate";

        public static const VEHICLETOOLTIP_VEHICLEISBROKEN:String = "#event:vehicleTooltip/vehicleIsBroken";

        public static const LOADING_EVENT_COMMON1_HEADER_1:String = "#event:loading/event/common1/header/1";

        public static const LOADING_EVENT_COMMON1_HEADER_2:String = "#event:loading/event/common1/header/2";

        public static const LOADING_EVENT_COMMON1_HEADER_3:String = "#event:loading/event/common1/header/3";

        public static const LOADING_EVENT_COMMON2_HEADER_1:String = "#event:loading/event/common2/header/1";

        public static const BOT_NAME_A36_SHERMAN_JUMBO_HELL:String = "#event:bot_name/A36_Sherman_Jumbo_HELL";

        public static const BOT_NAME_GB09_CHURCHILL_VII_HALL:String = "#event:bot_name/GB09_Churchill_VII_HALL";

        public static const BOT_NAME_A35_PERSHING_HELL:String = "#event:bot_name/A35_Pershing_HELL";

        public static const BOT_NAME_GB10_BLACK_PRINCE_HALL:String = "#event:bot_name/GB10_Black_Prince_HALL";

        public static const BOT_NAME_A63_M46_PATTON_HALL:String = "#event:bot_name/A63_M46_Patton_HALL";

        public static const BOT_NAME_GB11_CAERNARVON_HALL:String = "#event:bot_name/GB11_Caernarvon_HALL";

        public static const BOT_NAME_A68_T28_PROTOTYPE_HELL:String = "#event:bot_name/A68_T28_Prototype_HELL";

        public static const BOT_NAME_GB81_FV4004_HELL:String = "#event:bot_name/GB81_FV4004_HELL";

        public static const BOT_NAME_G25_PZII_LUCHS_HELL:String = "#event:bot_name/G25_PzII_Luchs_HELL";

        public static const BOT_NAME_G64_PANTHER_II_HALL_MINION:String = "#event:bot_name/G64_Panther_II_Hall_minion";

        public static const BOT_NAME_G54_E_50_HALL_MINION:String = "#event:bot_name/G54_E_50_Hall_minion";

        public static const BOT_NAME_G73_E50_AUSF_M_HALL_MINION:String = "#event:bot_name/G73_E50_Ausf_M_Hall_minion";

        public static const BOT_NAME_S17_EMIL_1952_E2_HALL:String = "#event:bot_name/S17_EMIL_1952_E2_Hall";

        public static const BOT_NAME_G00_BOMBER_HELL:String = "#event:bot_name/G00_Bomber_Hell";

        public static const BOT_NAME_G146_E100_HELL_BOSS:String = "#event:bot_name/G146_E100_Hell_Boss";

        public static const BOT_NAME_S29_UDES_14_5_HELL:String = "#event:bot_name/S29_UDES_14_5_HELL";

        public static const BOT_NAME_GB83_FV4005_HELL:String = "#event:bot_name/GB83_FV4005_HELL";

        public static const BOT_NAME_G114_RHEINMETALL_SKORPIAN_HELL:String = "#event:bot_name/G114_Rheinmetall_Skorpian_HELL";

        public static const BOT_NAME_IT09_P43_TER_HELL:String = "#event:bot_name/It09_P43_ter_HELL";

        public static const BOT_NAME_IT10_P43_BIS_HELL:String = "#event:bot_name/It10_P43_bis_HELL";

        public static const BOT_NAME_IT12_PROTOTIPO_STANDARD_B_HELL:String = "#event:bot_name/It12_Prototipo_Standard_B_HELL";

        public static const BOT_NAME_IT14_P44_PANTERA_HELL:String = "#event:bot_name/It14_P44_Pantera_HELL";

        public static const BOT_NAME_PL05_50TP_TYSZKIEWICZA_HELL:String = "#event:bot_name/Pl05_50TP_Tyszkiewicza_HELL";

        public static const BOT_NAME_PL10_40TP_HABICHA_HELL:String = "#event:bot_name/Pl10_40TP_Habicha_HELL";

        public static const BOT_NAME_PL11_45TP_HABICHA_HELL:String = "#event:bot_name/Pl11_45TP_Habicha_HELL";

        public static const BOT_NAME_PL13_53TP_MARKOWSKIEGO_HELL:String = "#event:bot_name/Pl13_53TP_Markowskiego_HELL";

        public static const BOT_NAME_S13_LEO_HELL:String = "#event:bot_name/S13_Leo_HELL";

        public static const BOT_NAME_S27_UDES_16_HELL:String = "#event:bot_name/S27_UDES_16_HELL";

        public static const BOT_NAME_S28_UDES_15_16_HELL:String = "#event:bot_name/S28_UDES_15_16_HELL";

        public static const BOT_NAME_A14_T30_HELL:String = "#event:bot_name/A14_T30_HELL";

        public static const BOT_NAME_G25_PZII_LUCHS_HELL_HALL:String = "#event:bot_name/G25_PzII_Luchs_HELL_HALL";

        public static const BOT_NAME_R77_KV2_TURRET_2:String = "#event:bot_name/R77_KV2_turret_2";

        public static const CONGRATULATION_CLOSE:String = "#event:congratulation/close";

        public static const CONGRATULATION_BACK:String = "#event:congratulation/back";

        public static const CONGRATULATION_TITLE:String = "#event:congratulation/title";

        public static const CONGRATULATION_PACK_0:String = "#event:congratulation/pack/0";

        public static const CONGRATULATION_PACK_1:String = "#event:congratulation/pack/1";

        public static const CONGRATULATION_FREE:String = "#event:congratulation/free";

        public static const CONGRATULATION_ADDITIONDESCRIPTION_0:String = "#event:congratulation/additionDescription/0";

        public static const CONGRATULATION_ADDITIONDESCRIPTION_1:String = "#event:congratulation/additionDescription/1";

        public static const IFT_HEADER:String = "#event:ift/header";

        public static const IFT_GROUPHEADER:String = "#event:ift/groupHeader";

        public static const IFT_AVAILABLE:String = "#event:ift/available";

        public static const IFT_UNAVAILABLE:String = "#event:ift/unavailable";

        public static const IFT_BACKBUTTON_GOTO:String = "#event:ift/backButton/goto";

        public static const IFT_TOKENS:String = "#event:ift/tokens";

        public static const IFT_TITLES_HE20_SHOP_ITEMS_BATTLEBOOSTERS:String = "#event:ift/titles/he20_shop_items_battleBoosters";

        public static const IFT_TITLES_HE20_SHOP_ITEMS_GOODIES:String = "#event:ift/titles/he20_shop_items_goodies";

        public static const IFT_TITLES_HE20_SHOP_ITEMS_CREWBOOKS:String = "#event:ift/titles/he20_shop_items_crewBooks";

        public static const EFFICIENCYRIBBONS_GOT_BUFF:String = "#event:efficiencyRibbons/got_buff";

        public static const EFFICIENCYRIBBONS_ARMOR_TITLE:String = "#event:efficiencyRibbons/armor/title";

        public static const EFFICIENCYRIBBONS_ARMOR_DESCR:String = "#event:efficiencyRibbons/armor/descr";

        public static const EFFICIENCYRIBBONS_MULTIPLYDAMAGEBY10_TITLE:String = "#event:efficiencyRibbons/multiplyDamageBy10/title";

        public static const EFFICIENCYRIBBONS_MULTIPLYDAMAGEBY10_DESCR:String = "#event:efficiencyRibbons/multiplyDamageBy10/descr";

        public static const EFFICIENCYRIBBONS_FUEL_TITLE:String = "#event:efficiencyRibbons/fuel/title";

        public static const EFFICIENCYRIBBONS_FUEL_DESCR:String = "#event:efficiencyRibbons/fuel/descr";

        public static const EFFICIENCYRIBBONS_REGENERATIONHP_TITLE:String = "#event:efficiencyRibbons/regenerationHP/title";

        public static const EFFICIENCYRIBBONS_REGENERATIONHP_DESCR:String = "#event:efficiencyRibbons/regenerationHP/descr";

        public static const EFFICIENCYRIBBONS_MULTIPLYGUNRELOADTIME_TITLE:String = "#event:efficiencyRibbons/multiplyGunReloadTime/title";

        public static const EFFICIENCYRIBBONS_MULTIPLYGUNRELOADTIME_DESCR:String = "#event:efficiencyRibbons/multiplyGunReloadTime/descr";

        public static const EFFICIENCYRIBBONS_RATION_TITLE:String = "#event:efficiencyRibbons/ration/title";

        public static const EFFICIENCYRIBBONS_RATION_DESCR:String = "#event:efficiencyRibbons/ration/descr";

        public static const EFFICIENCYRIBBONS_MULTIPLYSHOTDISPERSION_TITLE:String = "#event:efficiencyRibbons/multiplyShotDispersion/title";

        public static const EFFICIENCYRIBBONS_MULTIPLYSHOTDISPERSION_DESCR:String = "#event:efficiencyRibbons/multiplyShotDispersion/descr";

        public static const EFFICIENCYRIBBONS_DAMAGEONCEONSHOT_TITLE:String = "#event:efficiencyRibbons/damageOnceOnShot/title";

        public static const EFFICIENCYRIBBONS_DAMAGEONCEONSHOT_DESCR:String = "#event:efficiencyRibbons/damageOnceOnShot/descr";

        public static const EFFICIENCYRIBBONS_IGNITEONSHOT_TITLE:String = "#event:efficiencyRibbons/igniteOnShot/title";

        public static const EFFICIENCYRIBBONS_IGNITEONSHOT_DESCR:String = "#event:efficiencyRibbons/igniteOnShot/descr";

        public static const EFFICIENCYRIBBONS_HEALONCEONSHOT_TITLE:String = "#event:efficiencyRibbons/healOnceOnShot/title";

        public static const EFFICIENCYRIBBONS_HEALONCEONSHOT_DESCR:String = "#event:efficiencyRibbons/healOnceOnShot/descr";

        public static const EFFICIENCYRIBBONS_PLUS:String = "#event:efficiencyRibbons/plus";

        public static const NOTIFICATION_COLLECTMATTER:String = "#event:notification/collectMatter";

        public static const BATTLETIMER_COLLECTMATTER:String = "#event:battleTimer/collectMatter";

        public static const NOTIFICATION_DELIVERMATTER:String = "#event:notification/deliverMatter";

        public static const BATTLETIMER_DELIVERMATTER:String = "#event:battleTimer/deliverMatter";

        public static const NOTIFICATION_GETTOPOINT:String = "#event:notification/getToPoint";

        public static const BATTLETIMER_GETTOPOINT:String = "#event:battleTimer/getToPoint";

        public static const NOTIFICATION_WAITFORALLIES:String = "#event:notification/waitForAllies";

        public static const NOTIFICATION_FILLINGIN:String = "#event:notification/fillingIn";

        public static const NOTIFICATION_GOTOTOTELEPORT:String = "#event:notification/gotoToTeleport";

        public static const ARENA_WAITING_TITLE:String = "#event:arena/waiting/title";

        public static const ARENA_WAITING_MESSAGE:String = "#event:arena/waiting/message";

        public static const ARENA_DESCRIPTION_TITLE:String = "#event:arena/description/title";

        public static const ARENA_DESCRIPTION_MESSAGE:String = "#event:arena/description/message";

        public static const SQUAD_AFK_WARNING:String = "#event:squad/afk/warning";

        public static const ARENA_MARKER_NEED_MATTER:String = "#event:arena/marker/need_matter";

        public static const ARENA_MARKER_DELIVER_MATTER:String = "#event:arena/marker/deliver_matter";

        public static const ARENA_MARKER_GET_TO:String = "#event:arena/marker/get_to";

        public static const ARENA_PHASE:String = "#event:arena/phase";

        public function EVENT()
        {
            super();
        }
    }
}
