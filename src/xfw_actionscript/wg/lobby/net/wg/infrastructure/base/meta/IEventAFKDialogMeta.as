package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IEventAFKDialogMeta extends IEventDispatcher
    {

        function as_setData(param1:String) : void;
    }
}
