package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IEventBanInfoMeta extends IEventDispatcher
    {

        function as_setEventBanInfo(param1:Object) : void;

        function as_setVisible(param1:Boolean) : void;
    }
}
