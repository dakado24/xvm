package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IHE20EntryPointMeta extends IEventDispatcher
    {

        function onClickS() : void;

        function as_setData(param1:Object) : void;
    }
}
