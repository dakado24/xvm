package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IEventBrowserScreenMeta extends IEventDispatcher
    {

        function as_setBrowserPadding(param1:Boolean) : void;
    }
}
