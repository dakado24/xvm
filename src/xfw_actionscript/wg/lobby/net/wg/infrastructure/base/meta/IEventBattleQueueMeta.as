package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IEventBattleQueueMeta extends IEventDispatcher
    {

        function as_setDifficulty(param1:int) : void;
    }
}
