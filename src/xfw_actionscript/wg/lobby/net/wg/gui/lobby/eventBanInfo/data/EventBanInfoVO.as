package net.wg.gui.lobby.eventBanInfo.data
{
    import net.wg.gui.components.paginator.vo.ToolTipVO;

    public class EventBanInfoVO extends ToolTipVO
    {

        public var description:String = "";

        public function EventBanInfoVO(param1:Object)
        {
            super(param1);
        }
    }
}
