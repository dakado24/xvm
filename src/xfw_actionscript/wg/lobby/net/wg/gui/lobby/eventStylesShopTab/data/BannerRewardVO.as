package net.wg.gui.lobby.eventStylesShopTab.data
{
    import net.wg.data.daapi.base.DAAPIDataClass;

    public class BannerRewardVO extends DAAPIDataClass
    {

        public var name:String = "";

        public var count:String = "";

        public var icon:String = "";

        public function BannerRewardVO(param1:Object)
        {
            super(param1);
        }
    }
}
