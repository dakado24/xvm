package net.wg.gui.lobby.eventMessageWindow.data
{
    public class MessageContentResult extends Object
    {

        public static const UNDEFINED:int = 0;

        public static const CANCEL:int = 1;

        public static const OK:int = 2;

        public function MessageContentResult()
        {
            super();
        }
    }
}
