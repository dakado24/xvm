package net.wg.gui.lobby.eventStylesTrade.components
{
    import net.wg.gui.lobby.vehicleCustomization.controls.CarouselItemRenderer;

    public class EventCarouselItemRenderer extends CarouselItemRenderer
    {

        public function EventCarouselItemRenderer()
        {
            super();
            considerWidth = true;
        }
    }
}
