package net.wg.gui.lobby.eventShopConfirmation.data
{
    import net.wg.gui.components.paginator.vo.ToolTipVO;

    public class ConfirmationRewardVO extends ToolTipVO
    {

        public var icon:String = "";

        public var label:String = "";

        public function ConfirmationRewardVO(param1:Object)
        {
            super(param1);
        }
    }
}
