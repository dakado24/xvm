package net.wg.data.constants.generated
{
    public class DAILY_QUESTS_WIDGET_CONSTANTS extends Object
    {

        public static const WIDGET_LAYOUT_NORMAL:int = 0;

        public static const WIDGET_LAYOUT_MINI:int = 1;

        public static const WIDGET_LAYOUT_MICRO:int = 2;

        public function DAILY_QUESTS_WIDGET_CONSTANTS()
        {
            super();
        }
    }
}
