package net.wg.data.constants.generated
{
    public class SQUADTYPES extends Object
    {

        public static const SQUAD_TYPE_SIMPLE:String = "squadTypeSimple";

        public static const SQUAD_TYPE_EVENT:String = "squadTypeEvent";

        public static const SIMPLE_SQUAD_TEAM_SECTION:String = "SimpleSquadTeamSection_UI";

        public static const SIMPLE_SQUAD_CHAT_SECTION:String = "SimpleSquadChatSection_UI";

        public static const EVENT_SQUAD_TEAM_SECTION:String = "EventSquadTeamSectionUI";

        public static const EVENT_SQUAD_CHAT_SECTION:String = "EventSquadChatSectionUI";

        public function SQUADTYPES()
        {
            super();
        }
    }
}
